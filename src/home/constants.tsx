import * as React from "react"
import { Icon } from "antd"

import { AiOutlineHome } from "react-icons/ai"

export const homeIcon = <Icon component={AiOutlineHome} />
export const homeRoute = "/"
