import Title from "antd/lib/typography/Title";
import { Button } from "apprise-frontend/components/Button";
import { icns } from "apprise-frontend/icons";
import { Page } from "apprise-frontend/scaffold/Page";
import { DrawerContext } from "apprise-frontend/scaffold/Scaffold";
import { Sidebar } from "apprise-frontend/scaffold/Sidebar";
import { tagIcon } from "apprise-frontend/tag/constants";
import { tenantIcon, tenantRoute } from "apprise-frontend/tenant/constants";
import { useUsers } from "apprise-frontend/user/api";
import { userIcon, userRoute } from "apprise-frontend/user/constants";
import { givenChildren } from "apprise-frontend/utils/children";
import { aclogo } from 'emaris-frontend/constants';
import * as React from "react";
import { useTranslation } from "react-i18next";
import "./styles.scss";




export const Home = () => {

  const {t} = useTranslation()
  const {logged} = useUsers()

  const drawers = React.useContext(DrawerContext)

  const subtitle =  logged.hasNoTenant() ? t("ac_home.full_subtitle") :
                    logged.isTenantManager() ? 
                        t("ac_home.manager_subtitle") 
                    : t("ac_home.manager_subtitle") 

  const usersBlurb = <Blurb key={'usersBlurb'} color='darkgoldenrod' icon={userIcon} className="dashboard" title={t("ac_home.user_title")}>
                        <ul>
                          <li>{icns.dot}{t("ac_home.user_description_1")}</li>
                          <li>{icns.dot}{t("ac_home.user_description_2")}</li>
                          <li>{icns.dot}{t("ac_home.user_description_3")}</li>
                          <li>{icns.dot}{t("ac_home.user_description_4")}</li>
                        </ul>
                        <Button linkTo={userRoute}>{t("ac_home.user_button_1")}</Button>
                        <Button enabled={logged.isTenantManager() || logged.hasNoTenant()} linkTo={`${userRoute}/new`} >{t("ac_home.user_button_2")}</Button>
                      </Blurb>

  const tenantsBlurb = <Blurb key={'tenantsBlurb'} color='#0895c3' icon={tenantIcon} title={t("ac_home.tenant_title")}>
                        <ul>
                          <li>{icns.dot}{t("ac_home.tenant_description_1")}</li>
                          <li>{icns.dot}{t("ac_home.tenant_description_2")}</li>
                        </ul>
                        <Button linkTo={tenantRoute}>{t("ac_home.tenant_button_1")}</Button>
                        <Button linkTo={`${tenantRoute}/new`} >{t("ac_home.tenant_button_2")}</Button>
                      </Blurb>

  const tenantsManagerBlurb = <Blurb key={'tenantsManagerBlurb'} color='teal' icon={tenantIcon} title={t("ac_home.tenant_manager_title")}>
                        <ul>
                          <li>{icns.dot}{t("ac_home.tenant_manager_description_1")}</li>
                          <li>{icns.dot}{t("ac_home.tenant_manager_description_2")}</li>
                          <li>{icns.dot}{t("ac_home.tenant_manager_description_3")}</li>
                          <li>{icns.dot}{t("ac_home.tenant_manager_description_4")}</li>
                        </ul>
                        <Button linkTo={tenantRoute}>{t("ac_home.tenant_manager_button_1")}</Button>
                      </Blurb>

  const settingsBlurb = <Blurb key={'settingsBlurb'} color='lightseagreen' icon={tagIcon} title={t("ac_home.settings_title")}>
                      <ul>
                        <li>{icns.dot}{t("ac_home.settings_description_1")}</li>
                        <li>{icns.dot}{t("ac_home.settings_description_2")}</li>
                        <li>{icns.dot}{t("ac_home.settings_description_3")}</li>
                        <li>{icns.dot}{t("ac_home.settings_description_4")}</li>
                      </ul>
                      <Button onClick={()=>drawers.open("tags")}>{t("ac_home.settings_button_1")}</Button>
                    </Blurb>

  const blurbs = logged.hasNoTenant() ? [tenantsBlurb, usersBlurb, settingsBlurb] : [tenantsManagerBlurb, usersBlurb]
  
  

   return  <Page className="apppage" headerClassName="appheader">

        <Sidebar/>

        <Title className="app-title">
          {aclogo}
          <div className="app-name">{t("ac_home.title")}</div>
          <div className="app-subtitle">{subtitle}</div>
        </Title>

        <div className="app-blurb">
          {blurbs}
        </div>
 
      </Page>
  
}

type HomeEntryProps = React.PropsWithChildren<{
  className?: string
  icon: JSX.Element
  color?: string
  title: string
  buttons?: JSX.Element[]
}>

const Blurb = (props: HomeEntryProps) => {

  const { className, icon, color, title, children } = props

  const { btns, other } = givenChildren(children).byTypes([["btns", Button]]);

  return <div className={`entry ${className ?? ''}`}>
    <div className="innerEntry">
      <div className="logo">
        <div style={{ color }} className="icon-halo">
          {icon}
        </div>
      </div>
      <div className="contents">
        <div className="title">{title}</div>
        <div className="text">
          {/* <div className="subtitle">{subtitle}</div> */}
          {other}
        </div>
        {btns && <div className="buttons">{btns.map(b => React.cloneElement(b, { className: "button", size: 'large', style: { background:color} }))} </div>}
      </div>
    </div>
  </div >


}