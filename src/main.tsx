
import { layoutmodule } from "apprise-frontend/layout/module";
import { mailmodule } from 'apprise-frontend/mail/module';
import { tagmodule } from "apprise-frontend/tag/module";
import { tenantmodule } from "apprise-frontend/tenant/module";
import { usermodule } from "apprise-frontend/user/module";
import { messagemodule } from 'apprise-messages/module';
import { campaignmodule, compliancemodule, timelinessmodule } from "emaris-frontend/campaign/module";
import { submissionModule } from 'emaris-frontend/campaign/submission/module';
import { eventmodule } from "emaris-frontend/event/module";
import { productmodule } from "emaris-frontend/product/module";
import { requirementmodule } from "emaris-frontend/requirement/module";

import { state } from '#app';
import { AdminConsole } from '#index';
import { mockery } from '#mockery';
import { App } from 'apprise-frontend/App';
import { PushEvents } from 'apprise-frontend/push/PushEvents';
import ReactDOM from 'react-dom';


const modules = [usermodule, tenantmodule, tagmodule, eventmodule, requirementmodule, submissionModule, campaignmodule, productmodule, layoutmodule, compliancemodule, timelinessmodule, messagemodule, mailmodule]


ReactDOM.render(

    <App initState={state} modules={modules} mockery={mockery} >
        <PushEvents>
            <AdminConsole />
        </PushEvents>
    </App>

    , document.getElementById('root'));
