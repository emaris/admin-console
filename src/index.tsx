
import { homeIcon, homeRoute } from "#home/constants";
import { Home } from "#home/Home";
import { LogoutItem } from 'apprise-frontend/call/Logout';
import { ExternalLink } from "apprise-frontend/scaffold/ExternalLink";
import { Scaffold } from "apprise-frontend/scaffold/Scaffold";
import { Section } from "apprise-frontend/scaffold/Section";
import { Slider } from "apprise-frontend/scaffold/Slider";
import { Settings, settingsId, SettingsItem } from "apprise-frontend/settings/Settings";
import { Tags, tagsId, TagsItem } from "apprise-frontend/tag/List";
import { tenants } from 'apprise-frontend/tenant/api';
import { tenantIcon, tenantPlural, tenantRoute, tenantSingular } from "apprise-frontend/tenant/constants";
import { Tenants } from "apprise-frontend/tenant/Tenants";
import { useUsers } from "apprise-frontend/user/api";
import { userIcon, userPlural, userRoute } from "apprise-frontend/user/constants";
import { UserProfile, userprofileId, UserProfileItem } from "apprise-frontend/user/Profile";
import { Users } from "apprise-frontend/user/Users";
import { acbanner, AcIcon, iotcLogo, iotcRoute, McIcon, mcRoute, RcIcon, rcRoute } from "emaris-frontend/constants";
import { useTranslation } from "react-i18next";
import "./variables.css";


export const AdminConsole = () => {

    const { logged } = useUsers()

    const { t } = useTranslation()

    return <Scaffold title={t("ac_home.title")} shortTitle={t("ac_home.short_title")} icon={<AcIcon  color='white' />} banner={acbanner} >

        <Section title={t("ac_home.name")} icon={homeIcon} path={homeRoute} exact crumbs={{
            [homeRoute]: { name: '' }

        }}>
            <Home />
        </Section>

        <Section crumbs={{ 
            [tenantRoute]: { name: t(tenantPlural) },
            [`${tenantRoute}/new`]: { name: t('tenant.new') },
            [`${tenantRoute}/*`]: { resolver: tenants.breadcrumbResolver } 
        }}
            title={t(logged.hasNoTenant() || logged.isMultiManager() ? tenantPlural : tenantSingular)} icon={tenantIcon} path={tenantRoute}>
            <Tenants />
        </Section>

        <Section title={t(userPlural)} icon={userIcon} path={userRoute} crumbs={{ [userRoute]: { name: t(userPlural) }, [`${userRoute}/new`]: { name: t('user.account.new') } }}>
            <Users />
        </Section>

        <Slider id={userprofileId}>
            <UserProfile />
        </Slider>

        <Slider id={settingsId}>
            <Settings />
        </Slider>

        <Slider id={tagsId} enabled={logged.hasNoTenant()}>
            <Tags />
        </Slider>

        {UserProfileItem()}
        {TagsItem()}
        {SettingsItem()}
        {LogoutItem()}

        <ExternalLink title={t("links.iotc")} icon={iotcLogo} href={iotcRoute} />

        <ExternalLink enabled={logged.isTenantUser()} title={t("links.rc")} icon={<RcIcon />} href={rcRoute} />
        <ExternalLink enabled={logged.hasNoTenant()} title={t("links.mc")} icon={<McIcon />} href={mcRoute} />


    </Scaffold>

}
